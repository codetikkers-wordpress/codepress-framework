<?php

namespace Codepress\Framework;

class WordPressLoader
{
    /**
     * Load WordPress core files in an environment ready only.
     * For example: load WordPress from a custom command, ...
     */
    public function load(): void
    {
        $table_prefix = config(
            'database.connections.mysql.prefix',
            'ct_',
        );

        require web_path('wordpress/wp-settings.php');
    }
}
