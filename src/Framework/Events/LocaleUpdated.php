<?php

namespace Codepress\Framework\Events;

class LocaleUpdated
{
	/**
	 * The updated application locale.
	 *
	 * @var string
	 */
	public $locale;

	public function __construct($locale)
	{
		$this->locale = $locale;
	}
}
