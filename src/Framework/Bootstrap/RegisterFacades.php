<?php

namespace Codepress\Framework\Bootstrap;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Facade;
use Codepress\Framework\AliasLoader;
use Codepress\Framework\PackageManifest;

class RegisterFacades
{
	public function bootstrap(Application $app)
	{
		Facade::clearResolvedInstances();

		Facade::setFacadeApplication($app);

        AliasLoader::getInstance(array_merge(
            $app->make('config')->get('app.aliases', []),
            $app->make(PackageManifest::class)->aliases(),
        ))->register();
    }
}
