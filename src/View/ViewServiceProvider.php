<?php namespace Codepress\View;

use Illuminate\View\ViewServiceProvider as IlluminateServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;

class ViewServiceProvider extends IlluminateServiceProvider
{
	/**
	 * Register view bindings
	 */
	public function register ()
	{
		parent::register();
	}
}