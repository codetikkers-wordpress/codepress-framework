<?php

if (!function_exists('is_subpage')) {
    /**
     * Determine if current WordPress condition is a sub-page (of).
     *
     * @param int|string|array $parent
     *
     * @return bool
     */
    function is_subpage($parent)
    {
        global $post;

        if (is_null($post)) {
            return false;
        }

        if (empty($parent)) {
            if (is_page() && $post->post_parent > 0) {
                return true;
            }
        } else {
            $parentPost = get_post($post->post_parent);

            if (is_numeric($parent) && is_page() && (int)$parent == $post->post_parent) {
                return true;
            } elseif (is_string($parent) && is_page()) {
                if (is_a($parentPost, 'WP_Post') && $parent === $parentPost->post_name) {
                    return true;
                }
            } elseif (is_array($parent) && is_page()) {
                if (in_array($parentPost->ID, $parent, true) || in_array($parentPost->post_name, $parent, true)) {
                    return true;
                }
            }
        }

        return false;
    }
}

if (!function_exists('load_application_textdomain')) {
    /**
     * Register the .mo file for the application text domain translations.
     *
     * @param string $domain
     * @param string $locale
     *
     * @return bool
     * @throws ErrorException
     *
     */
    function load_application_textdomain(string $domain, string $locale)
    {
        if (!function_exists('load_textdomain')) {
            throw new ErrorException(
                'Function called too early. Function depends on the {load_textdomain} WordPress function.',
            );
        }

        $path = resource_path('languages' . DS . $locale . DS . $domain . '.mo');

        if (file_exists($path) && is_readable($path)) {
            return load_textdomain($domain, $path);
        }

        return false;
    }
}

if (!function_exists('load_codepress_plugin_textdomain')) {
    /**
     * Register the .mo file for any codepress plugins. Work for extensions
     * installed inside the "plugins" and "mu-plugins" directories.
     *
     * @param string $domain
     * @param string $path
     *
     * @return bool
     */
    function load_codepress_plugin_textdomain(string $domain, string $path)
    {
        /**
         * Filters a plugin's locale.
         *
         * @param string $locale The plugin's current locale.
         * @param string $domain Text domain. Unique identifier for retrieving translated strings.
         *
         * @since 3.0.0
         */
        $locale = apply_filters('plugin_locale', determine_locale(), $domain);

        $mofile = $domain . '-' . $locale . '.mo';

        return load_textdomain($domain, rtrim($path, '\/') . DIRECTORY_SEPARATOR . $mofile);
    }
}

if (!function_exists('meta')) {
    /**
     * Retrieve metadata for the specified object.
     *
     * @param int $object_id
     * @param string $meta_key
     * @param bool $single
     * @param string $meta_type
     *
     * @return mixed
     */
    function meta($object_id, $meta_key = '', $single = false, $meta_type = 'post')
    {
        return get_metadata($meta_type, $object_id, $meta_key, $single);
    }
}

if (!function_exists('muplugins_path')) {
    /**
     * Return the mu-plugins path.
     *
     * @param string $path
     *
     * @return string
     */
    function muplugins_path($path = '')
    {
        return app()->mupluginsPath($path);
    }
}

if (!function_exists('plugins_path')) {
    /**
     * Return the plugins path.
     *
     * @param string $path
     *
     * @return string
     */
    function plugins_path($path = '')
    {
        return app()->pluginsPath($path);
    }
}

if (!function_exists('themes_path')) {
    /**
     * Get the path to the themes folder.
     *
     * @param string $path
     *
     * @return string
     */
    function themes_path($path = '')
    {
        return app()->themesPath($path);
    }
}

if (!function_exists('web_path')) {
    /**
     * Get the public web path.
     *
     * @param string $path
     *
     * @return string
     */
    function web_path($path = '')
    {
        return app()->webPath($path);
    }
}